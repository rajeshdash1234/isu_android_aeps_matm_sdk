package com.matm.matmsdk;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import isumatm.androidsdk.equitas.R;

public class DemoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo);
    }
}
